module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: ["public/css/", "public/js/scripts.js",  "public/js/scripts.min.js", "public/img/"],
    imagemin: {
      dynamic: {
        options: {
          optimizationLevel: 3
        },
        files: [{
          expand: true,
          cwd: 'img/',
          src: ['*.{png,jpg,gif}'],
          dest: 'public/img/'
        }]
      }
    },
    less: {
      development: {
        options: {
          paths: ['less'],
          yuicompress: false
        },
        files: {
          'less/screen.css':'less/screen.less'
        }
      }
    },
    autoprefixer: {
      dist: {
        files: {
            'public/css/screen.css': 'less/screen.css'
        }
      }
    },
    cssmin: {
      minify: {
        expand: true,
        cwd: 'public/css/',
        src: ['*.css', '!*.min.css'],
        dest: 'public/css/',
        ext: '.min.css'
      }
    },
    concat: {
      dist: {
        src: [
          'js/main.js'
        ],
        dest: 'public/js/scripts.js'
      }
    },
    uglify: {
      target: {
        // options: {
        //   beautify: true
        // },
        files: {
          'public/js/scripts.min.js': ['public/js/scripts.js']
        }
      }
    },
    validation: {
      options: {
        reset: grunt.option('reset') || true,
        stoponerror: false,
        relaxerror: ["Bad value X-UA-Compatible for attribute http-equiv on element meta.","document type does not allow element \"[A-Z]+\" here"]
      },
      your_target: {
          src: ['public/*.html']
      }
    },
    watch: {
      css: {
        files: 'less/**/*.less',
        tasks: ['less','autoprefixer'],
        options: {
            livereload: true
        }
      },
      js: {
        files: 'js/*.js',
        tasks: ['concat']
      },
      html: {
        files: 'public/*.html',
        tasks: ['validation']
      },
      images: {
        files: 'img/*.{png,jpg,gif}',
        tasks: ['imagemin']
      }
    },
    connect: {
        server: {
            options: {
                port: 9001,
                base: 'public',
                keepalive: true
            }
        }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-html-validation');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.registerTask('pro',['clean','less','autoprefixer','concat','cssmin','uglify','imagemin']);
};
